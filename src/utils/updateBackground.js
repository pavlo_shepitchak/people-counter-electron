// @flow
import config from '../config';
import sharp from 'sharp';
export default async (
  background: Sharp,
  frame: Sharp,
  segments: Array<SegmentData>
) => {
  const { width, height, channels } = await background.metadata();
  const convertToCoordinates = (index: number): Pixel => [
    Math.ceil(index / width),
    index % width
  ];
  const isInMovingArea = (index: number) => {
    const [y, x] = convertToCoordinates(Math.floor(index / 3));
    const offset = 10;
    return segments.some(
      ({ bounds: { top, bottom, left, right } }) =>
        y > top - offset &&
        y < bottom + offset &&
        x > left - offset &&
        x < right + offset
    );
  };
  const backgroundRaw = await background
    .clone()
    .raw()
    .toBuffer();
  const frameRaw = await frame
    .clone()
    .raw()
    .toBuffer();
  const result = backgroundRaw.map(
    (pixel, index) =>
      !isInMovingArea(index) ? refreshPixel(pixel, frameRaw[index]) : pixel
  );

  return await sharp(result, {
    raw: { width, height, channels }
  }).png();
};
const refreshPixel = (prevPixel: number, nextPixel: number) =>
  prevPixel * (1 - config.backgroundRefreshCoefficient) +
  nextPixel * config.backgroundRefreshCoefficient;
