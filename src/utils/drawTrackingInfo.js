// @flow
import { createCanvas, Image } from 'canvas';
import config from '../config';
import sharp from 'sharp';
export default async (
  image: Sharp,
  trackedObjects: Array<TrackedObject>,
  counter: Counter
) => {
  const { width, height } = await image.metadata();
  const canvas = createCanvas(width, height);
  const ctx = canvas.getContext('2d');

  const img = new Image();
  img.src = await image.toBuffer();
  ctx.drawImage(img, 0, 0);
  trackedObjects.forEach(item => {
    const {
      //mask,
      center,
      bounds,
      id
    } = item;
    drawId(ctx, id, center);
    drawRectangle(ctx, bounds);
    drawCenter(ctx, center);
  });
  const legend = trackedObjects
    .map(({ id, weight, timer, state, peopleNumber }) =>
      [
        `id: ${id}`,
        `weight: ${weight}`,
        `timer: ${timer}`,
        `state: ${state}`,
        `people ${peopleNumber}`
      ].join(',\n')
    )
    .join('\n\n');
  drawText(ctx, legend);
  drawLine(ctx, config.barrier, width);
  drawCounter(ctx, counter, width);
  return sharp(canvas.toBuffer());
};
const drawRectangle = (ctx: CanvasRenderingContext2D, bounds: Bounds) => {
  const { left, right, top, bottom } = bounds;
  ctx.strokeStyle = 'rgba(80, 244, 66,1 )';
  ctx.strokeRect(left, top, right - left, bottom - top);
};
const drawCenter = (ctx: CanvasRenderingContext2D, center: Center) => {
  ctx.strokeStyle = 'rgba(80, 244, 66,1 )';
  ctx.beginPath();
  ctx.arc(center[1], center[0], 2, 0, Math.PI * 2, true);
  ctx.stroke();
};
const drawText = (
  ctx: CanvasRenderingContext2D,
  text: string
  //center: Center
) => {
  ctx.textBaseline = 'top';
  ctx.font = '12px Arial';
  ctx.fillStyle = 'red';
  ctx.textAlign = 'left';
  ctx.fillText(text, 0, 0);
};
const drawId = (ctx: CanvasRenderingContext2D, id: number, center: Center) => {
  ctx.textBaseline = 'top';
  ctx.font = '12px Arial';
  ctx.fillStyle = 'red';
  ctx.textAlign = 'center';
  ctx.fillText(`id: ${id}`, center[1], center[0]);
};
const drawLine = (
  ctx: CanvasRenderingContext2D,
  position: number,
  width: number
) => {
  ctx.beginPath();
  ctx.moveTo(0, position);
  ctx.lineTo(width, position);
  ctx.strokeStyle = 'red';
  ctx.stroke();
};
const drawCounter = (
  ctx: CanvasRenderingContext2D,
  {up, down}: Counter,
  position: number
) => {
  const text = `UP: ${up}\nDOWN: ${down}`;
  ctx.textBaseline = 'top';
  ctx.font = '12px Arial';
  ctx.fillStyle = 'red';
  ctx.textAlign = 'right';
  ctx.fillText(text, position, 0);
};
