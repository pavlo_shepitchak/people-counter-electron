// @flow
export default async (image: Sharp): Promise<string> => {
  const buffer = await image.toBuffer();
  const base64Flag = 'data:imagePath/jpeg;base64,';
  const ba = buffer.toString('base64');
  return base64Flag + ba;
};
