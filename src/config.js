const zoom = 3;
const height = 240;
const width = 320;
const humanSizeRelative = 0.06; // human weight/image weight
const brushSizeRelative = 8 * 10 ** 5;
const imageWeight = width * height;
const humanSize = imageWeight * humanSizeRelative;
const morphBrushSize = imageWeight / brushSizeRelative;
console.log(morphBrushSize);

export default {
  diffThreshold: 10,
  imageRange: [1080, 1160],
  morphBrushSize: 3,
  humanSize: humanSize,
  barrier: height / 2,
  idRange: [1, 100],
  maxObjectStep: 40,
  backgroundRefreshCoefficient: 0.1,
  fps: 8,
  step: 3
};
