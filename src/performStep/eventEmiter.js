// @flow
import events from 'events';
import counter from './counter';
export const event = {
  COUNTER_UP: 'COUNTER_UP',
  COUNTER_DOWN: 'COUNTER_DOWN',
  COUNTER_UPDATE: 'COUNTER_UPDATE'
};

const eventEmitter = new events.EventEmitter();

eventEmitter.on(event.COUNTER_UP, () => counter.increaseCounterUp());
eventEmitter.on(event.COUNTER_DOWN, () => counter.increaseCounterDown());
eventEmitter.on(event.COUNTER_UPDATE, state => {
  if (state === 'DOWN') {
    eventEmitter.emit(event.COUNTER_DOWN);
  } else if (state === 'UP') {
    eventEmitter.emit(event.COUNTER_UP);
  }
});

export default eventEmitter;
