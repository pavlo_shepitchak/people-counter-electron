// @flow
import sharp from 'sharp';
import config from '../../config';
import { flatten } from 'lodash';
import imageToYuv from './imageToYuv';
export default async (image1: Sharp, image2: Sharp) => {
  //console.time('all');
  // console.time('imageToYuv');
  const image1Yuv = await imageToYuv(image1.clone());
  const image2Yuv = await imageToYuv(image2.clone());
  // console.timeEnd('imageToYuv');
  //console.time('applyBetweenVectors');
  let pixels = applyBetweenVectors(image1Yuv, image2Yuv, subtractYukPixels);
  //console.timeEnd('applyBetweenVectors');
  //console.time('sharp');
  pixels = Buffer.from(flatten(pixels));
  const { width, height } = await image1.metadata();
  const result = await sharp(pixels, {
    raw: { width, height, channels: 1 }
  })
    .greyscale()
    .png()
    .threshold(config.diffThreshold);
  //console.timeEnd('sharp');
  //console.timeEnd('all');

  return result;
};

const subtractYukPixels = (p1: Array<number>, p2: Array<number>) => {
  const [yDiff, uDiff, vDiff] = subtractArrays(p1, p2);
  return uDiff + vDiff + (yDiff > 80 ? yDiff : 0);
};
const subtractArrays = (a1: Array<number>, a2: Array<number>): Array<number> =>
  applyBetweenVectors(a1, a2, (first, second) => Math.abs(first - second));

const applyBetweenVectors = <T>(
  first: Array<T>,
  second: Array<T>,
  func: (T, T) => any
): Array<T> => first.map((item, index) => func(item, second[index]));
