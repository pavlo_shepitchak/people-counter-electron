// @flow
import subtractImages from './subtractImages';
import removeNoise from './removeNoise';
import segmentation from './segmentation';

export default async (frame: Sharp, background: Sharp) => {
  const diff = await subtractImages(background, frame); //TODO slow
  const withoutNoise = await removeNoise(diff);
  const segments = await segmentation(withoutNoise);
  return { diff, withoutNoise, segments}
};
