// @flow
export default (outer: TrackedObject, inner: TrackedObject): boolean => {
  const [y, x] = inner.center;
  const { top, bottom, right, left } = outer.bounds;
  return (
    y > top &&
    y < bottom &&
    x > left &&
    x < right &&
    inner.peopleNumber < outer.peopleNumber
  );
};
