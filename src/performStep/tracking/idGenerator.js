// @flow
import { range } from 'lodash';
import config from '../../config';

const idSet = range(...config.idRange);
export const generateId = (): number => {
  return idSet.shift();
};
export const releaseId = (id: number) => idSet.push(id);
