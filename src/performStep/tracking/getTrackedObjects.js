// @flow
import config from '../../config';
import eventEmitter, { event } from '../eventEmiter';
import correctNewLost from './correctNewLost';
import getState from './getState';
import { generateId } from './idGenerator';
const _history = [];
const addToHistory = (frame: Array<TrackedObject>): number =>
  _history.push(frame);
const getLastHistoryFrame = (): Array<TrackedObject> =>
  _history[_history.length - 1] || [];

const findObjectInSet = (object: SegmentData, set: Array<TrackedObject>) =>
  set.find(
    item =>
      getDistance(item.center, object.center) < config.maxObjectStep &&
      item.peopleNumber === object.peopleNumber
  );
const getDistance = (pixel1: Center, pixel2: Center): number => {
  const [yd, xd] = [
    Math.abs(pixel1[0] - pixel2[0]),
    Math.abs(pixel1[1] - pixel2[1])
  ];
  return Math.sqrt(yd ** 2 + xd ** 2);
};

export default (segments: Array<SegmentData>): Array<TrackedObject> => {
  const prevFrame = getLastHistoryFrame();
  const nextFrame = segments.map(segment => {
    const fromPrevFrame = findObjectInSet(segment, prevFrame);
    const state = !fromPrevFrame
      ? 'NONE'
      : getState(fromPrevFrame.center, segment.center);
    if (state !== 'NONE') {
      eventEmitter.emit(event.COUNTER_UPDATE, state);
    }
    if (fromPrevFrame) {
      return {
        ...segment,
        id: fromPrevFrame.id,
        timer: state === 'NONE' ? fromPrevFrame.timer : 10,
        state
      };
    }
    return {
      ...segment,
      id: generateId(),
      timer: 0,
      state: 'NONE'
    };
  });
  let corrected = correctNewLost(prevFrame, nextFrame);
  corrected = corrected.map(item => ({
    ...item,
    timer:
      item.state !== 'NONE' ? 10 : item.timer > 0 ? item.timer - 1 : item.timer
  }));
  addToHistory(corrected);
  return corrected;
};
