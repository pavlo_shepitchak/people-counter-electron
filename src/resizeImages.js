const fl = require('node-filelist');
const sharp = require('sharp');
const files = '../frames';
const option = { ext: 'jpeg|jpg|png|gif' };

fl.read([files], option, function(results) {
  const names = results.map(item => item.path.split('/').pop());
  names.forEach(item =>
    sharp(`${files}/${item}`)
      .withMetadata()
      .resize(150)
      .toFile(`../resized/${item}`)
  );
});
