// @flow
import sharp from 'sharp';
import { range } from 'lodash';
import config from './config';
import performStep from './performStep';
import { drawTrackingInfo, join4 } from './utils';
const inputFolder = 'frames';

export default async function(callback: Sharp => void) {
  const indexes = range(...config.imageRange);
  const _background = await sharp(`${inputFolder}/img${980}.png`);
  while (indexes.length) {
    const index = indexes.shift();
    if (index % config.step === 0) {
      console.log(index);
      const nextFrame = await sharp(`${inputFolder}/img${index}.png`);
      const {
        frame,
        objects,
        counterValue,
        background,
        difference,
        withoutNoise
      } = await performStep(nextFrame, _background);
      const draw = await drawTrackingInfo(frame, objects, counterValue);
      const toShow = await join4([draw, background, difference, withoutNoise]);
      callback && callback(toShow);
    }
  }
}
