// @flow
declare type Sharp = {
  greyscale: void => Sharp,
  raw: void => Sharp,
  toBuffer: void => Buffer,
  png: void => Sharp,
  threshold: number => Sharp,
  clone: void => Sharp,
  overlayWith: (Buffer, ?{}) => Sharp,
  extend: number => Sharp,
  metadata: void => Promise<{ height: number, width: number, channels: number }>
};
type Pixel = [number, number];

type Bounds = {
  top: number,
  bottom: number,
  left: number,
  right: number
};
type Center = [number, number];
type SegmentData = {
  mask: Sharp,
  center: Center,
  bounds: Bounds,
  weight: number,
  peopleNumber: number
};
type State = 'DOWN' | 'UP' | 'NONE';

type TrackedObject = SegmentData & {
  id: number,
  timer: number,
  state: State
};
type Counter = {
  up: number,
  down: number
};
declare module 'sharp' {
  declare module.exports: (string | Buffer | null, ?{}) => Sharp;
}
declare module 'canvas' {
  declare module.exports: {
    createCanvas: any,
    Image: any
  };
}
